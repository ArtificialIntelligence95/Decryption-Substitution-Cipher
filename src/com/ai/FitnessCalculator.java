package com.ai;

import java.util.HashMap;
import java.util.Map;

public class FitnessCalculator {
    private static Map<String, Integer> scoreTable;

    static {
        scoreTable = new HashMap<>();
        scoreTable.put("TH", 2);
        scoreTable.put("HE", 1);
        scoreTable.put("IN", 1);
        scoreTable.put("ER", 1);
        scoreTable.put("AN", 1);
        scoreTable.put("ED", 1);
        scoreTable.put("THE", 5);
        scoreTable.put("ING", 5);
        scoreTable.put("AND", 5);
        scoreTable.put("EEE", -5);

//        scoreTable.put("�S", 10);
//        scoreTable.put("�RE", 10);
//        scoreTable.put(" A ", 10);
//        scoreTable.put(" I ", 10);
    }

    public static int calc(String in) {
        int fitnessScore = 0;

        for (String key: scoreTable.keySet()) {
            int occurrence = Helper.numberOfOccurrence(in, key);
            fitnessScore += scoreTable.get(key) * occurrence;
        }
        return fitnessScore;
    }
}