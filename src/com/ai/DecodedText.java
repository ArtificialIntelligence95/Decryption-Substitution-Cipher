package com.ai;

public class DecodedText implements Comparable {

    private String text;
    private String key;
    private int fitness;

    public DecodedText(String text, String key, int fitness) {
        this.text = text;
        this.key = key;
        this.fitness = fitness;
    }

    public String getText() {
        return text;
    }

    public String getKey() {
        return key;
    }

    public int getFitness() {
        return fitness;
    }


    @Override
    public int compareTo(Object o) {
        DecodedText other = (DecodedText) o;
        if (this.fitness < other.fitness)
            return 1;
        else if (this.fitness == other.fitness)
            return 0;
        else
            return -1;
    }
}
