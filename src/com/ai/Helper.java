package com.ai;

import java.util.ArrayList;
import java.util.List;

public class Helper {
    public static int numberOfOccurrence(String base, String find) {
        int lastIndex = 0;
        int count = 0;

        while (lastIndex != -1) {
            lastIndex = base.indexOf(find, lastIndex);
            if (lastIndex != -1) {
                count++;
                lastIndex += find.length();
            }
        }
        return count;
    }

    public static String swapIndices(String base, int i, int j) {
        StringBuilder stringBuilder = new StringBuilder(base);
        stringBuilder.replace(i, i + 1, base.charAt(j) + "");
        stringBuilder.replace(j, j + 1, base.charAt(i) + "");
        return stringBuilder.toString();
    }
    public static List<String> permutation(String str) {
        List<String> permutations = new ArrayList<>();
        permutation("", str, permutations);
        return permutations;
    }

    private static void permutation(String prefix, String str, List<String> permutaions) {
        int n = str.length();
        if (n == 0)
             permutaions.add(prefix);
        else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n), permutaions);
        }
    }
}
