package com.ai;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateRandomSequence {
    private static final String base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String mostFrequent = "ETAOINSHRDLCUMWFGYPBVKJXQZ";
    private static final int NUM_OF_MOST_USED = 5;

    public static String generate() {
        Random random = new Random();

        char a[] = base.toCharArray();

        for (int i = 0; i < a.length; i++) {
            int j = random.nextInt(a.length);
            char temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
        return new String(a);
    }

    public static List<String> generate(List<LetterSorter.Letter> occurrence) {
        List<String> keys = new ArrayList<>();

        String mostUsedInEnglish = "";
        for (int i = 0; i < NUM_OF_MOST_USED; i++) {
            mostUsedInEnglish += mostFrequent.charAt(i);
        }
        List<String> permutations = Helper.permutation(mostUsedInEnglish);
        for (String permutation : permutations) {
            //System.out.println(permutation);
            String rand = generate();
            for (int i = 0; i < NUM_OF_MOST_USED; i++) {
                rand = Helper.swapIndices(
                        rand,
                        permutation.charAt(i)-'A',
                        rand.indexOf(occurrence.get(i).getCharacter())
                );
            }
            keys.add(rand);
        }
        return keys;
    }

}