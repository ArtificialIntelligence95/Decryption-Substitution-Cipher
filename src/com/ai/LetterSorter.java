package com.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LetterSorter {
    List<Letter> letters;

    public List<Letter> sort(String input) {
        letters = new ArrayList<>();

        int[] chars = new int[26];
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isLetter(c)) {
                chars[c - 'A']++;
            }
        }

        for (int i = 0; i < 26; i++) {
            letters.add(new Letter((char)(i + 'A'), chars[i]));
        }

        Collections.sort(letters);

        return letters;
    }

    class Letter implements Comparable {
        private char character;
        private int occurrences;

        public Letter(char character, int occurrences) {
            this.character = character;
            this.occurrences = occurrences;
        }

        public char getCharacter() {
            return character;
        }

        public int getOccurrences() {
            return occurrences;
        }

        @Override
        public int compareTo(Object o) {
            Letter other = (Letter) o;
            if (this.occurrences < other.occurrences)
                return 1;
            else if (this.occurrences == other.occurrences)
                return 0;
            else
                return -1;
        }
    }
}
