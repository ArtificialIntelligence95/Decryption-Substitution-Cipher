package com.ai;

import java.util.HashMap;

public class CypherDecoder {
    public String decode(String encodedText, String key) {
        HashMap<Character, Character> keyMap = new HashMap<>();
        StringBuilder decodedText = new StringBuilder();
        for (int i = 0; i < key.length(); i++) {
            keyMap.put(key.charAt(i), (char) ('A' + i));
        }
//        System.out.println(key);
//        System.out.println(keyMap.toString());
        for (int i = 0; i < encodedText.length(); i++) {
            char c = encodedText.charAt(i);
            if (Character.isLetter(c)){
                decodedText.append(keyMap.get(c));
            }
                else
                decodedText.append(c);
        }
        return decodedText.toString();
    }
}
