package com.ai;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static final String INPUT_FILE_ADDRESS = "EncryptedText";

    public static final int MAX_TRIES = 300;
    public static final int STARTING_NUMBER_OF_KEYS = 200;
    public static final int NUMBER_OF_GOOD_KEY_ITER = 20;
    public static final int NUM_OF_MUTATION = 3;
    public static int NUMBER_OF_KEYS_SELECTED = 3;

    public static void main(String[] args) {
        String fileStr = null;
//        System.out.println(Helper.swapIndices("ArdavanBozorgi",0,14));
        try {
            fileStr = readFile(INPUT_FILE_ADDRESS, Charset.forName("UTF8"));
            System.out.println(fileStr.length());
            System.out.println(fileStr.substring(0, 10));
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<LetterSorter.Letter> letters = new ArrayList<>();
        LetterSorter letterSorter = new LetterSorter();
        letters = letterSorter.sort(fileStr);
        System.out.println(letters.get(0).getCharacter());
        System.out.println(letters.get(1).getCharacter());
        System.out.println(letters.get(2).getCharacter());
        System.out.println(letters.get(3).getCharacter());
        System.out.println(letters.get(4).getCharacter());

        //System.out.println(goodKeys.toString());

        CypherDecoder cypherDecoder = new CypherDecoder();
        FitnessCalculator fitnessCalculator = new FitnessCalculator();

        List<DecodedText> decodedTexts = new ArrayList<>();
        List<String> newKeys = new ArrayList<>();

//        for (int i = 0; i < STARTING_NUMBER_OF_KEYS; i++) {
//            newKeys.add(GenerateRandomSequence.generate());
//        }
        for (int i = 0; i < NUMBER_OF_GOOD_KEY_ITER; i++) {
            List<String> goodKeys = GenerateRandomSequence.generate(letters);
            for (String goodKey : goodKeys) {
                newKeys.add(goodKey);
                System.out.println(goodKey);
            }
        }

//        newKeys.add("WOPFGLKADBCMNEIHJZQYXRTSVU");
        for (int numOfTries = 0; numOfTries < MAX_TRIES; numOfTries++) {
            decodedTexts.clear();
            for (int i = 0; i < newKeys.size(); i++) {
                String decodedString = cypherDecoder.decode(fileStr, newKeys.get(i));
                int fitness = fitnessCalculator.calc(decodedString);
                decodedTexts.add(new DecodedText(decodedString, newKeys.get(i), fitness));
            }
            Collections.sort(decodedTexts);

//        for (int i = 0; i < 10; i++) {
//            System.out.println(decodedTexts.get(i).getFitness());
//        }

            if (numOfTries < 5)
                NUMBER_OF_KEYS_SELECTED = 10;
            else
                NUMBER_OF_KEYS_SELECTED = 3;

            newKeys.clear();
            for (int i = 0; i < NUMBER_OF_KEYS_SELECTED; i++) {
                System.out.println(decodedTexts.get(i).getFitness());
                newKeys.add(decodedTexts.get(i).getKey());
                for (int j = 0; j < NUMBER_OF_KEYS_SELECTED; j++) {
                    if (i == j) continue;
                    newKeys.add(crossOver(decodedTexts.get(i).getKey(), decodedTexts.get(j).getKey()));
                    newKeys.add(crossOver2(decodedTexts.get(i).getKey(), decodedTexts.get(j).getKey()));
                }
            }

            int startSize = newKeys.size();
            for (int i = 0; i < startSize; i++) {
                newKeys.add(mutate(newKeys.get(i)));
            }
            System.out.println("________________________________");
        }

        System.out.println("result = " + decodedTexts.get(0).getText().substring(0, 100));
        System.out.println("result = " + decodedTexts.get(0).getKey());
    }

    static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private static String mutate(String key) {
        Random random = new Random();

        StringBuilder stringBuilder = new StringBuilder(key);

//        boolean[] usedChars = new boolean[26];
//        String s = key;
//        for (int i = 0; i < 26; i++) {
//            usedChars[i] = false;
//        }
//        for (int i = 0; i < key.length(); i++) {
//            usedChars[key.charAt(i) - 'A'] = true;
//        }
//        boolean res = true;
//        for (int i = 0; i < 26; i++) {
//            res &= usedChars[i];
//        }

        for (int count = 0; count < NUM_OF_MUTATION; count++) {
            int i = random.nextInt(key.length());
            int j = random.nextInt(key.length());
            char c1 = stringBuilder.charAt(i);
            char c2 = stringBuilder.charAt(j);
            stringBuilder.replace(i, i + 1, c2 + "");
            stringBuilder.replace(j, j + 1, c1 + "");
        }
        return stringBuilder.toString();
    }

    private static String crossOver(String key1, String key2) {
        String newKey = "";

        boolean[] usedChars = new boolean[26];
        for (int i = 0; i < 26; i++) {
            usedChars[i] = false;
        }
        for (int i = 0; i < 13; i++) {
            char c = key1.charAt(i);
            usedChars[c - 'A'] = true;
            newKey += c;
        }
        for (int i = 13; i < 26; i++) {
            char c = key2.charAt(i);
            if (usedChars[c - 'A'])
                newKey += '0';
            else {
                newKey += c;
                usedChars[c - 'A'] = true;
            }
        }

        for (int i = 0; i < 26; i++)
            if (!usedChars[i])
                newKey = newKey.replaceFirst("0", (char) (i + 'A') + "");

        return newKey;
    }

    private static String crossOver2(String key1, String key2) {
        String newKey = "";

        boolean[] usedChars = new boolean[26];
        for (int i = 0; i < 26; i++) {
            usedChars[i] = false;
        }
        for (int i = 0; i < 26; i++) {
            char c;
            if (i % 2 == 0)
                c = key1.charAt(i);
            else
                c = key2.charAt(i);

            if (usedChars[c - 'A'])
                newKey += '0';
            else {
                newKey += c;
                usedChars[c - 'A'] = true;
            }
        }

        for (int i = 0; i < 26; i++)
            if (!usedChars[i])
                newKey = newKey.replaceFirst("0", (char) (i + 'A') + "");

        return newKey;
    }
}
